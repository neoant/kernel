= CPU
John Doe <iocdr@protonmail.com>

== Related Work

* Lozi, Jean-Pierre, et al. "The Linux scheduler: a decade of wasted cores."
  Proceedings of the Eleventh European Conference on Computer Systems. ACM,
  2016.
* Zhao, Yong, et al. "Preemptive Multi-Queue Fair Queuing." Proceedings of the
  28th International Symposium on High-Performance Parallel and Distributed
  Computing. ACM, 2019.
