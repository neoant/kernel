// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! # The Kernel for The NeoAnt Project
//!
//! The NeoAnt Project aims to develop a modern operating system that is
//! extensible, safe, and performant. It plans to accomplish this goal by
//! combining cutting-edge research in Operating Systems, Security, and
//! Programming Languages.
//!
//! The Kernel for The NeoAnt Project is an Unified Extensible Firmware
//! Interface (UEFI) executable. It can be executed directly by an UEFI
//! compliant firmware.

#![feature(core_intrinsics)]
#![deny(warnings)]
#![no_main]
#![no_std]

mod uefi;

/// `#[panic_handler]` is used to define the behavior of `panic!` in
/// `#![no_std]` applications.
#[panic_handler]
pub fn panic(_info: &core::panic::PanicInfo) -> ! {
    core::intrinsics::abort()
}

/// EFI_IMAGE_ENTRY_POINT
///
/// The most significant parameter that is passed to an image is a pointer to
/// the System Table. This pointer is EFI_IMAGE_ENTRY_POINT, the main entry
/// point for a UEFI Image. The System Table contains pointers to the active
/// console devices, a pointer to the Boot Services Table, a pointer to the
/// Runtime Services Table, and a pointer to the list of system configuration
/// tables such as ACPI, SMBIOS, and the SAL System Table.
///
/// # Summary
///
/// This is the main entry point for a UEFI Image. This entry point is the same
/// for UEFI applications and UEFI drivers.
///
/// # Parameters
///
/// * ImageHandle - The firmware allocated handle for the UEFI image.
/// * SystemTable - A pointer to the EFI System Table.
///
/// # Description
///
/// This function is the entry point to an EFI image. An EFI image is loaded and
/// relocated in system memory by the EFI Boot Service
/// EFI_BOOT_SERVICES.LoadImage(). An EFI image is invoked through the EFI Boot
/// Service EFI_BOOT_SERVICES.StartImage().
///
/// The first argument is the image's image handle. The second argument is a
/// pointer to the image's system table. The system table contains the standard
/// output and input handles, plus pointers to the EFI_BOOT_SERVICES and
/// EFI_RUNTIME_SERVICES tables. The service tables contain the entry points in
/// the firmware for accessing the core EFI system functionality. The handles in
/// the system table are used to obtain basic access to the console. In
/// addition, the System Table contains pointers to other standard tables that a
/// loaded image may use if the associated pointers are initialized to nonzero
/// values. Examples of such tables are ACPI, SMBIOS, SAL System Table,etc.
///
/// The ImageHandle is a firmware-allocated handle that is used to identify the
/// image on various functions. The handle also supports one or more protocols
/// that the image can use. All images support the EFI_LOADED_IMAGE_PROTOCOL and
/// the EFI_LOADED_IMAGE_DEVICE_PATH_PROTOCOL that returns the source location
/// of the image, the memory location of the image, the load options for the
/// image, etc.
///
/// If the UEFI image is a UEFI OS Loader, then the UEFI OS Loader executes and
/// either returns, calls the EFI Boot Service Exit(), or calls the EFI Boot
/// Service EFI_BOOT_SERVICES.ExitBootServices(). If the EFI OS Loader returns
/// or calls Exit(), then the load of the OS has failed, and the EFI OS Loader
/// is unloaded from memory and control is returned to the component that
/// attempted to boot the UEFI OS Loader. If ExitBootServices() is called, then
/// the UEFI OS Loader has taken control of the platform, and EFI will not
/// regain control of the system until the platform is reset. One method of
/// resetting the platform is through the EFI Runtime Service ResetSystem().
///
/// # Status Codes Returned
///
/// * EFI_SUCCESS - The driver was initialized.
/// * EFI_OUT_OF_RESOURCES - The request could not be completed due to a lack of
///   resources.
#[no_mangle]
pub extern "C" fn efi_main(
    image_handle: uefi::Handle,
    system_table: &uefi::SystemTable,
) -> uefi::Status {
    // A UEFI OS loader must ensure that it has the system's current memory map
    // at the time it calls ExitBootServices(). This is done by passing in the
    // current memory map’s MapKey value as returned by
    // EFI_BOOT_SERVICES.GetMemoryMap(). Care must be taken to ensure that the
    // memory map does not change between these two calls. It is suggested that
    // GetMemoryMap() be called immediately before calling ExitBootServices().

    let mut memory_map_size: usize = 0;
    let mut memory_map: core::mem::MaybeUninit<uefi::MemoryDescriptor> =
        core::mem::MaybeUninit::<uefi::MemoryDescriptor>::uninit();
    let mut descriptor_size: usize = 0;
    let mut map_key: usize = 0;
    let mut descriptor_version: u32 = 0;

    loop {
        match (system_table.boot_services.get_memory_map)(
            &mut memory_map_size,
            memory_map.as_mut_ptr(),
            &mut map_key,
            &mut descriptor_size,
            &mut descriptor_version,
        ) {
            uefi::Status::SUCCESS => {
                break;
            }
            uefi::Status::BUFFER_TOO_SMALL => {
                match (system_table.boot_services.allocate_pool)(
                    uefi::MemoryType::LoaderData,
                    memory_map_size,
                    &mut (memory_map.as_mut_ptr() as *mut core::ffi::c_void),
                ) {
                    uefi::Status::SUCCESS => {
                        continue;
                    }
                    _ => {
                        return uefi::Status::OUT_OF_RESOURCES;
                    }
                }
            }
            _ => {
                return uefi::Status::OUT_OF_RESOURCES;
            }
        }
    }

    // If MapKey value is incorrect, ExitBootServices() returns
    // EFI_INVALID_PARAMETER and GetMemoryMap() with ExitBootServices() must be
    // called again. Firmware implementation may choose to do a partial shutdown
    // of the boot services during the first call to ExitBootServices(). A UEFI
    // OS loader should not make calls to any boot service function other than
    // GetMemoryMap() after the first call to ExitBootServices().

    match (system_table.boot_services.exit_boot_services)(image_handle, map_key) {
        uefi::Status::SUCCESS => {}
        _ => {
            return uefi::Status::OUT_OF_RESOURCES;
        }
    }

    // The ExitBootServices() function is called by the currently executing UEFI
    // OS loader image to terminate all boot services. On success, the UEFI
    // OS loader becomes responsible for the continued operation of the system.

    (system_table.runtime_services.reset_system)(
        uefi::ResetType::ResetShutdown,
        uefi::Status::SUCCESS,
        0,
        core::ptr::null(),
    );

    return uefi::Status::SUCCESS;
}
