// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Helper application to test kernel in qemu OVMF

#![deny(warnings)]

use std::io::Write;

fn main() -> std::io::Result<()> {
    // Parse command line arguments
    let args: Vec<String> = std::env::args().collect();
    assert!(args.len() == 2);

    // Create the image file
    let named_image_file = tempfile::NamedTempFile::new()?;
    let (image_file, image_pathbuf) = named_image_file.keep()?;

    // Set image size
    image_file.set_len(10 * 0x100_000)?;

    // Format image as FAT32
    fatfs::format_volume(&image_file, fatfs::FormatVolumeOptions::new())?;

    // Open the FAT32 file system
    let fs = fatfs::FileSystem::new(&image_file, fatfs::FsOptions::new())?;

    // Create run.efi
    let mut efi = fs.root_dir().create_file("run.efi")?;
    efi.truncate()?;
    let efi_path = std::path::Path::new(&args[1]);
    let efi_contents = std::fs::read(efi_path)?;
    efi.write_all(&efi_contents)?;

    // Create startup.nsh
    let mut startup_nsh = fs.root_dir().create_file("startup.nsh")?;
    startup_nsh.truncate()?;
    startup_nsh.write_all(b"fs0:\nrun.efi")?;

    // Run qemu
    std::process::Command::new("/usr/bin/qemu-system-x86_64")
        .args(&[
            "-drive".into(),
            format!(
                "file={},index=0,media=disk,format=raw",
                image_pathbuf.display()
            ),
        ])
        .args(&["-bios", "/usr/share/edk2/ovmf/OVMF_CODE.fd"])
        .args(&["-net", "none"])
        .spawn()?;

    Ok(())
}
